package PT2018.demo.DemoProject;

//package simulation;

import java.util.concurrent.atomic.AtomicInteger;

//import ttt.Controller;
//import ttt.Scheduler;
//import ttt.Task;



public class Simulator implements Runnable {
	
	public static int clientsLoadperServer=5;
	public static int maxNoServers;
	public static int minArrivalInterval;
	public static int maxArrivalInterval;
	public static int minServiceTime;
	public static int maxServiceTime;
	public static int finishTime;
	public static int noOfOpenServers = 0;
	private AtomicInteger nrClient=new AtomicInteger();
	private Scheduler scheduler;
	private SimulatorFrame frame;
    private int[] averageWaitingTime=new int[10];
    private int[] averageServiceTime=new int[10];
    private int[] peakHour=new int[10];
    private int[] clientsPerServer=new int[10];

	public  Simulator() {
		scheduler = new Scheduler();
		frame = new SimulatorFrame();
	}

	@Override
	public  void run() {
        int peak=0;
		int currTime = 0;
		int stop=0;
		while (currTime <finishTime) {
			
			int serviceTime = (int) (Math.random() * (maxServiceTime - minServiceTime) + minServiceTime);
            int arrivalInterval=(int) (Math.random() * (maxArrivalInterval - minArrivalInterval) + minArrivalInterval);
            currTime=currTime+1;
            stop +=arrivalInterval;
            Task t = new Task(stop, serviceTime);
          
            nrClient.set(nrClient.get()+1);
		    t.setNrClient(nrClient.get());
			scheduler.dispatchTaskOnServer(t);
			t.toString();
			
			frame.displayData(scheduler.getCurrentTasks(),currTime);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		if (currTime>=finishTime){
			int i=0;
		
			for (Task[] t:scheduler.getAllTasks()){
				 averageWaitingTime[i]=0;
				 averageServiceTime[i]=0;
				 clientsPerServer[i]=0;
				System.out.println("Logger of server no"+i);
				for (Task task:t){
					System.out.println(task.toString());
					clientFrequency(task);
					int aux=task.getTimpFinal()-task.getTimpSosire();
					averageWaitingTime[i]+=aux;
					averageServiceTime[i]+=task.getTimpPrelucrare();
					clientsPerServer[i]++;
				}
				averageWaitingTime[i]=averageWaitingTime[i]/clientsPerServer[i];
				averageServiceTime[i]=averageServiceTime[i]/clientsPerServer[i];
				System.out.println("Average waiting time: "+Integer.toString(averageWaitingTime[i]));
				System.out.println("Average service time: "+Integer.toString(averageServiceTime[i]));
				System.out.println("No clients per server: "+Integer.toString(clientsPerServer[i]));
		
			i++;
			
			}
			System.out.println("Peak hour: "+Integer.toString(peak(peakHour)));
		    peak=peak(peakHour);
		}
		while (scheduler.isEmpty()==false){
			frame.displayData(scheduler.getCurrentTasks(),currTime);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		frame.displayStatistics(averageWaitingTime,averageServiceTime,peak);
		

	}
	public  int peak(int[] peakHour){
		int max=Integer.MIN_VALUE;
		for (int i=0;i<peakHour.length;i++){
			if (peakHour[i]>max) max=i;
		}
		return max;
	}
	public  void clientFrequency(Task t){
		int hours=3;
		for (int i=1;i<hours;i++){
			if ((i*60>t.getTimpSosire())&&(t.getTimpSosire()>(i-1)*60)){
				peakHour[i]+=1;
			}
		}
	}
	public  SimulatorFrame getSimulatorFrame(){
		return frame;
	}

}