package PT2018.demo.DemoProject;

//package ttt;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {

	private Task tsk;
	private BlockingQueue<Task> blkq;
	private BlockingQueue<Task> allTasks;
	private AtomicInteger waitingTime;

	public  Server() {
		waitingTime = new AtomicInteger(0);
		blkq = new LinkedBlockingQueue<>();
		 allTasks= new LinkedBlockingQueue<>();
	}

	public  void run() {
		while (true) {
			Task t;
			try {
				t = blkq.take();
				Thread.sleep(t.getTimpPrelucrare()*1000);
				waitingTime.addAndGet((-1) * t.getTimpPrelucrare());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
   public  int getWaitingTime(){
	   return waitingTime.get();
   }
	public  void addTask(Task t) {
		blkq.add(t);
		allTasks.add(t);
		tsk=t;
		waitingTime.addAndGet(t.getTimpPrelucrare());

	}
	
	public  Task[] getTasks(){
		
		Task[] task = new Task[blkq.size()];
		blkq.toArray(task);
		
		return task;
	}
public  Task[] getAllTasks(){
		
		Task[] allTaskArray = new Task[allTasks.size()];
		allTasks.toArray(allTaskArray );
		
		return allTaskArray ;
	}
	public  boolean isEmpty(){
		return blkq.isEmpty();
	}
	public  Task getTail(){
		return tsk;
	}
	
}
